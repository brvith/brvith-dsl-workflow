/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import org.slf4j.LoggerFactory


class ParallelSteps<SI : StepInput, SO : StepOutput>(private val name: String) : Step<SI, SO> {
    private val log = LoggerFactory.getLogger(ParallelSteps::class.java)!!

    private var steps: MutableList<Step<SI, SO>> = mutableListOf()

    fun addStep(step: Step<SI, SO>) {
        steps.add(step)
    }

    override fun name(): String {
        return name
    }

    override suspend fun execute(input: SI): SO {
        var stepStatus = WorkflowConstants.STATUS_SUCCESS

        val results = coroutineScope {
            val deferred = steps.map { step ->
                async {
                    step.execute(input)
                }
            }
            deferred.awaitAll()
        }
        val output = StatusResolverFactory.instance<SO>().deriveStatus(results)
        log.info("Executed : ${name()}, status :${output.status}")
        return output
    }
}