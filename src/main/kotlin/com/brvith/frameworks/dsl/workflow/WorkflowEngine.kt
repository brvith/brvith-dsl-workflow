/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow

import org.slf4j.LoggerFactory
import javax.script.ScriptEngineManager

object WorkflowConstants {
    const val STATUS_SUCCESS = "success"
    const val STATUS_FAILURE = "failure"
}

interface WorkflowFlowEngine<WC, SI : StepInput, SO : StepOutput> {
    suspend fun execute(dslContent: String, workflowContext: WC, stepInput: SI)
    suspend fun execute(workflow: Step<SI, SO>, workflowContext: WC, stepInput: SI)

}

interface Step<SI : StepInput, SO : StepOutput> {

    fun name(): String

    suspend fun execute(input: SI): SO
}

interface StatusResolver<SO : StepOutput> {
    fun deriveStatus(stepOutput: SO): SO
    fun deriveStatus(stepOutputs: List<SO>): SO
}

class WorkFlowEngineFactory {
    companion object {
        fun <WC : WorkflowContext, SI : StepInput, SO : StepOutput> instance(name: String)
                : WorkflowFlowEngine<WC, SI, SO> {
            return WorkFlowEngineImpl()
        }
    }
}

class WorkFlowEngineImpl<WC : WorkflowContext, SI : StepInput, SO : StepOutput>
    : WorkflowFlowEngine<WC, SI, SO> {

    override suspend fun execute(dslContent: String, workflowContext: WC, stepInput: SI) {
        val ktsEngine = ScriptEngineManager().getEngineByExtension("kts")
        checkNotNull(ktsEngine) { "failed to get kotlin script engine" }

        with(ktsEngine) {
            val scriptResult = eval(dslContent)
            val sequentialFlow = scriptResult as SequenceSteps<SI, SO>
            execute(sequentialFlow, workflowContext, stepInput)
        }
    }

    override suspend fun execute(workflow: Step<SI, SO>, context: WC, stepInput: SI) {
        // It may Workflow or Single Step
        workflow.execute(stepInput)
    }


}

class StepFactory {
    companion object {
        fun <SI : StepInput, SO : StepOutput> instance(name: String): Step<SI, SO> {
            return DefaultStep(name)
        }
    }
}

class DefaultStep<SI : StepInput, SO : StepOutput>(private val name: String) : Step<SI, SO> {
    private val log = LoggerFactory.getLogger(DefaultStep::class.java)!!

    override fun name(): String {
        return name
    }

    override suspend fun execute(t: SI): SO {
        log.info("Executing Step : $name")
        return StepOutput().apply { status = WorkflowConstants.STATUS_SUCCESS } as SO
    }
}

class StatusResolverFactory {
    companion object {
        fun <SO : StepOutput> instance(): StatusResolver<SO> {
            return DefaultStatusResolver()
        }
    }
}

class DefaultStatusResolver<SO : StepOutput> : StatusResolver<SO> {

    override fun deriveStatus(stepOutput: SO): SO {
        return stepOutput
    }

    override fun deriveStatus(stepOutputs: List<SO>): SO {

        val statuses = stepOutputs.map { it.status }
        val stepStatus = if (statuses.contains(WorkflowConstants.STATUS_FAILURE)) {
            WorkflowConstants.STATUS_FAILURE
        } else {
            WorkflowConstants.STATUS_SUCCESS
        }
        return StepOutput().apply { status = stepStatus } as SO
    }
}

