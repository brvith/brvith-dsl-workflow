/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow

import org.slf4j.LoggerFactory

class SequenceSteps<SI : StepInput, SO : StepOutput>(private val name: String) : Step<SI, SO> {
    private val log = LoggerFactory.getLogger(SequenceSteps::class.java)!!

    private var steps: MutableList<Step<SI, SO>> = mutableListOf()

    fun addStep(step: Step<SI, SO>) {
        steps.add(step)
    }

    override fun name(): String {
        return name
    }

    override suspend fun execute(input: SI): SO {
        var stepOutput = StepOutput().apply { status = "success" }
        log.info("Executing Sequence : ${name()}")
        steps.forEach { step ->
            if (step is ConditionalSteps) {
                // Store the previous output
                step.predicate = stepOutput.status
            }
            stepOutput = step.execute(input)
        }
        val output = StatusResolverFactory.instance<SO>().deriveStatus(stepOutput as SO)
        log.info("Executed : ${name()}, status :${output.status}")
        return output
    }
}