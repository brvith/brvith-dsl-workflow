/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow

import org.slf4j.LoggerFactory

class ConditionalSteps<SI : StepInput, SO : StepOutput>(private val name: String) : Step<SI, SO> {
    private val log = LoggerFactory.getLogger(ConditionalSteps::class.java)!!

    // Derived by the previous Step
    lateinit var predicate: String
    var successSteps: SequenceSteps<SI, SO> = SequenceSteps("$name-${WorkflowConstants.STATUS_SUCCESS}")
    var failureSteps: SequenceSteps<SI, SO> = SequenceSteps("$name-${WorkflowConstants.STATUS_FAILURE}")

    override fun name(): String {
        return name
    }

    override suspend fun execute(input: SI): SO {
        log.info("Predicate : $predicate")
        return when (predicate) {
            WorkflowConstants.STATUS_FAILURE -> {
                log.info("Executing failed : ${name()}")
                failureSteps.execute(input)
            }
            else -> {
                log.info("Executing Success : ${name()}")
                successSteps.execute(input)
            }
        }
    }
}