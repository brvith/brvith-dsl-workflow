/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow


fun <SI : StepInput, SO : StepOutput> workflow(name: String, block: WorkflowBuilder<SI, SO>.() -> Unit): Step<SI, SO> = WorkflowBuilder<SI, SO>(name).apply(block).build()

class WorkflowBuilder<SI : StepInput, SO : StepOutput>(name: String) {
    private lateinit var sequenceSteps: SequenceSteps<SI, SO>

    fun steps(name: String, block: SequenceStepBuilder<SI, SO>.() -> Unit) {
        sequenceSteps = SequenceStepBuilder<SI, SO>(name).apply(block).build()
    }

    fun build(): Step<SI, SO> = sequenceSteps
}

class SequenceStepBuilder<SI : StepInput, SO : StepOutput>(name: String) {
    private var sequenceSteps = SequenceSteps<SI, SO>(name)

    fun step(name: String) {
        sequenceSteps.addStep(StepFactory.instance(name))
    }

    fun parallelSteps(name: String, block: ParallelStepsBuilder<SI, SO>.() -> Unit) {
        val parallelSteps = ParallelStepsBuilder<SI, SO>(name).apply(block).build()
        sequenceSteps.addStep(parallelSteps)
    }

    fun conditionalSteps(name: String, block: ConditionalStepBuilder<SI, SO>.() -> Unit) {
        val conditionalSteps = ConditionalStepBuilder<SI, SO>(name).apply(block).build()
        sequenceSteps.addStep(conditionalSteps)
    }

    fun build(): SequenceSteps<SI, SO> = sequenceSteps
}

class ParallelStepsBuilder<SI : StepInput, SO : StepOutput>(name: String) {
    private var parallelSteps = ParallelSteps<SI, SO>(name)

    fun step(name: String) {
        parallelSteps.addStep(StepFactory.instance(name))
    }

    fun build(): Step<SI, SO> = parallelSteps
}

class ConditionalStepBuilder<SI : StepInput, SO : StepOutput>(name: String) {
    private var conditionalSteps = ConditionalSteps<SI, SO>(name)

    fun successSteps(name: String, block: SequenceStepBuilder<SI, SO>.() -> Unit) {
        conditionalSteps.successSteps = SequenceStepBuilder<SI, SO>(name).apply(block).build()
    }

    fun failureSteps(name: String, block: SequenceStepBuilder<SI, SO>.() -> Unit) {
        conditionalSteps.failureSteps = SequenceStepBuilder<SI, SO>(name).apply(block).build()
    }

    fun build(): Step<SI, SO> = conditionalSteps
}



