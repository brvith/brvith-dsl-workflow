/*
 * Copyright © 2019 Brvith Solutions.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.brvith.frameworks.dsl.workflow

import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.nio.charset.Charset
import javax.script.ScriptEngineManager
import kotlin.test.assertNotNull

class WorkflowEngineTest {

    @Test
    fun testDSLWorkFlow() {
        val sequentialFlow = workflow<StepInput, StepOutput>("sample-workflow") {
            steps("sample-workflow") {
                step("sample-steps")
                parallelSteps("sample-parallel") {
                    step("step1")
                    step("step2")
                }
                conditionalSteps("sample-conditional") {
                    successSteps("condition-success") {
                        step("step3")
                        step("step4")
                        parallelSteps("inner-parallel") {
                            step("ip-step3")
                            step("ip-step4")
                        }
                    }
                    failureSteps("condition-failure") {
                        step("step5")
                        step("step6")
                    }
                }
            }
        }
        runBlocking {
            val workflowContext = WorkflowContext()
            val workFlowEngine = WorkFlowEngineFactory
                    .instance<WorkflowContext, StepInput, StepOutput>("brvith-workflow-engine")
            val stepInput = StepInput()
            workFlowEngine.execute(sequentialFlow, workflowContext, stepInput)
        }
    }

    @Test
    fun testTextDSLWorkFlow() {
        runBlocking {
            val ktsEngine = ScriptEngineManager().getEngineByExtension("kts")

            with(ktsEngine) {
                val script = javaClass.getResource("/sample-dsl-workflow.kts").readText(Charset.defaultCharset())
                        .trimIndent()

                val scriptResult = eval(script)
                val sequentialFlow = scriptResult as SequenceSteps<StepInput, StepOutput>
                assertNotNull(sequentialFlow, "failed to get script instance")

                val workflowContext = WorkflowContext()
                val workFlowEngine = WorkFlowEngineFactory
                        .instance<WorkflowContext, StepInput, StepOutput>("brvith-workflow-engine")
                val stepInput = StepInput()
                workFlowEngine.execute(script, workflowContext, stepInput)
            }
        }
    }
}